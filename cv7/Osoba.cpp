//
// Created by xproch17 on 09.11.2018.
//

#include "Osoba.h"

Osoba::Osoba(std::string jmeno, std::string rodneCislo){
    setJmeno(jmeno);
    setRodneCislo(rodneCislo);
}

std::string Osoba::getJmeno(){
    return m_jmeno;
}

std::string Osoba::getRodneCislo(){
    return m_rodneCislo;
}

void Osoba::setJmeno(std::string jmeno){
    if (jmeno != ""){
        m_jmeno = jmeno;
    } else {
        std::cout << "Nelze ulozit prazdne jmeno" << std::endl;
    }
}

void Osoba::setRodneCislo(std::string rodneCislo){
    if (rodneCislo != ""){
        m_rodneCislo = rodneCislo;
    } else {
        std::cout << "Nelze ulozit prazdne RC" << std::endl;
    }
}
