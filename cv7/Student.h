//
// Created by xproch17 on 09.11.2018.
//

#ifndef CV07_STUDENT_H
#define CV07_STUDENT_H

#include <iostream>
#include "Osoba.h"

class Student : public Osoba {
    int m_semestr;
    float m_prumer;
public:
    Student(std::string jmeno, std::string rodneCislo,
            int semestr, float prumer);
    void zvysSemestr();
    void printInfo();
    float getPrumer();
    int getSemestr();
    void setPrumer(float prumer);
};


#endif //CV07_STUDENT_H
