//
//  main.cpp
//  cviceni
//
//  Created by David Prochazka on 25/10/2018.
//  Copyright © 2018 David Prochazka. All rights reserved.
//
 
#include <iostream>
#include <array>
using namespace std;
 
 
class Kontejner{
    float m_vaha;
    string m_obsah;
    string m_majitel;
public:
    Kontejner(float vaha, string obsah, string majitel){
        m_vaha = vaha;
        m_obsah = obsah;
        m_majitel = majitel;
    }
 
    float getVaha(){
        return m_vaha;
    }
 
    string getObsah(){
        return m_obsah;
    }
 
    string getMajitel(){
        return m_majitel;
    }
 
    void printInfo(){
        cout << "Majitel: " << m_majitel << ", obsah: " << m_obsah << ", vaha: " << m_vaha << endl;
    }
};
 
class Patro {
    string m_oznaceni;
    array<Kontejner*, 10> m_pozice;
 
public:
    Patro(string oznaceni){
        m_oznaceni = oznaceni;
        // pozor, pokud chci menit pri vyctovem for cyklu hodnotu, ktera je v poli, tak musim
        // napsat pred nazev promenne &. V tento okamzik se neprovadi kopiie promenne v poli
        // coz je defaultni chovani, ale pracuji s referenci na puvodni hodnotu. Toto
        // lze teoreticky udelat u jakekoliv promenne. Je to de facto "zastupce" na puvodni
        // hodnotu. Nekdy se to dela i pro vyssi performance, ale tyto veci se resi v CPP.
        for(Kontejner* &pozice:m_pozice){
            pozice = nullptr;
        }
    }
 
    void vypisObsah(){
        cout << "\nObsah patra " << m_oznaceni << endl;
 
        // Pri vypisu, uz neni & potreba, klidne at se pracuje s kopii.
        for(Kontejner* pozice:m_pozice){
            if (pozice != nullptr){
                pozice->printInfo();
            }
        }
    }
 
    void ulozDoSkladu(int pozice, Kontejner* ukladanyKontejner){
        if (m_pozice.at(pozice) == nullptr){
            m_pozice.at(pozice) = ukladanyKontejner;
        } else {
            cout << "Bohuzel pozice " << pozice << " je jiz obsazena" << endl;
            cout << "Je tam kontejner, ktery vlastni " << m_pozice.at(pozice)->getMajitel() << endl;
        }
    }
 
    void odeberZeSkladu(int pozice){
        if(m_pozice.at(pozice) != nullptr){
            m_pozice.at(pozice) = nullptr;
        } else {
            cout << "Bohuzel pozice " << pozice << " je prazdna" << endl;
        }
    }
};
 
 
class Sklad{
    array<Patro*, 5> m_patra;
 
public:
    Sklad(){
        int poradoveCislo = 0;
        // nebo klasickym forem
        for(Patro* &patro:m_patra){
            string oznaceni = "Patro" + to_string(poradoveCislo);
            poradoveCislo += 1;
            patro = new Patro(oznaceni);
        }
    }
 
    ~Sklad(){
        for(Patro* &patro:m_patra){
            delete patro;
        }
    }
 
    void vypisObsah(){
        cout << "\nObsah skladu " << endl;
 
        for(Patro* patro:m_patra){
            if (patro != nullptr){
                patro->vypisObsah();
            }
        }
    }
 
    void ulozDoSkladu(int patro, int pozice, Kontejner* ukladanyKontejner){
        m_patra.at(patro)->ulozDoSkladu(pozice, ukladanyKontejner);
    }
 
    void odeberZeSkladu(int patro, int pozice){
        m_patra.at(patro)->odeberZeSkladu(pozice);
    }
};
 
 
int main() {
    // test pro prvni cast prikladu
    Patro* prvniPatro = new Patro("XB01");
    Kontejner* pocitace = new Kontejner(150.6, "Pocitace", "David");
    Kontejner* auta = new Kontejner(1150.6, "Auta", "Petr");
 
    prvniPatro->ulozDoSkladu(0, auta);
    prvniPatro->ulozDoSkladu(0, pocitace);
    prvniPatro->odeberZeSkladu(2);
 
    prvniPatro->vypisObsah();
 
    delete pocitace;
    delete prvniPatro;
    return 0;
}