#include <iostream>
#include <array>

using namespace std;

class Kontejner{

    float m_vaha;
    string m_obsah;
    string m_majitel;

public:
    Kontejner(float vaha, string obsah, string majitel){
        m_vaha = vaha;
        m_majitel = majitel;
        m_obsah = obsah;
    }

    float getVaha(){
        return m_vaha;
    }

    string getObsah(){
        return m_obsah;
    }

    string getMajitel(){
        return m_majitel;
    }
};

class Patro{
    string m_oznaceni;
    array<Kontejner*,5> m_kontejnery;

public:
    Patro(string oznaceni){
        m_oznaceni = oznaceni;
        // na kazdou pozici zapisu nic (null)
        for(Kontejner* &k : m_kontejnery){
            k = nullptr;
        }
    }

    void uloz(int pozice, Kontejner* k){
        m_kontejnery[pozice] = k;
    }

    void odeber(int pozice){
        m_kontejnery[pozice] = nullptr;
    }

    void vypisObsah(){
        for(Kontejner* k : m_kontejnery){
            if(k != nullptr) {
                cout << k->getMajitel() << " " << k->getObsah() << " " << k->getVaha() << endl;
            }
            else{
                cout << "Na teto pozici nic neni" << endl;
            }
        }
    }
};

int main() {
    Patro* p = new Patro("12B");
    Kontejner* k1 = new Kontejner(100,"Obleceni","Firma XYZ");
    Kontejner* k2 = new Kontejner(100,"Elektronika","Firma ABC");
    p->uloz(0,k1);
    p->uloz(1,k2);
    p->vypisObsah();
    return 0;
}