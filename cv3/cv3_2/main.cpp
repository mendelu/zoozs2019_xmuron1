#include <iostream>

// mikulasmuron.cz/wiki/images/3/37/Cv3.pdf
using namespace std;
class Developer {
private:
    string m_jmeno;
    float m_zakladni_mzda;
    int m_pocet_hodin;
    int m_bonus_za_prescas;
    int m_pocet_chyb;
    int m_srazka_za_chybu;
public:
    Developer(string jmeno,float zakladni_mzda){
        m_jmeno = jmeno;
        m_zakladni_mzda = zakladni_mzda;
        m_pocet_hodin = 0;
        m_bonus_za_prescas = 500;
        m_pocet_chyb = 0;
        m_srazka_za_chybu = 1500;
    }

    Developer(string jmeno,float zakladni_mzda,int pocet_hodin,int bonus_za_prescas){
        m_jmeno = jmeno;
        m_zakladni_mzda = zakladni_mzda;
        m_pocet_hodin = pocet_hodin;
        m_bonus_za_prescas = bonus_za_prescas;
        m_pocet_chyb = 0;
        m_srazka_za_chybu = 1500;
    }

    void pridejChybu(){
        m_pocet_chyb = m_pocet_chyb + 1;
    }


    void pracuj(int pocet_hodin){
        m_pocet_hodin = m_pocet_hodin + pocet_hodin;
    }

    void resetujMesic(){
        m_pocet_hodin = 0;
        m_pocet_chyb = 0;
    }

    float getMzda(){
        return m_zakladni_mzda + getBonusZaPrescas() - getSrazkaZaChyby();
    }

    void setZakladniMzda(float nova_mzda){
        m_zakladni_mzda = nova_mzda;
    }
private:
    float getSrazkaZaChyby(){
        return m_pocet_chyb * m_srazka_za_chybu;
    }

    float getBonusZaPrescas(){
        if(m_pocet_hodin > 40){
            return ((m_pocet_hodin-40) * m_bonus_za_prescas);
        }
        return 0;
    }
};
int main() {
    Developer* d1 = new Developer("Mikulas",50000,50,500);
    d1->pracuj(20);
    d1->pridejChybu();
    d1->pridejChybu();
    cout << d1->getMzda() << endl;
    d1->pracuj(10);
    cout << d1->getMzda() << endl;
    return 0;
}