#include <iostream>
using namespace std;
class Student{
private:
    string m_jmeno;
public:
    Student(string jmeno){
        m_jmeno = jmeno;
    }

    string getJmeno() {
        return m_jmeno;
    }

    void setJmeno(string jmeno){
        if(jmeno.size() > 1){
            m_jmeno = jmeno;
        }
    }
};


int main() {
    Student* s1 = new Student("Mikulas");
    cout << s1->getJmeno() << endl;
    s1->setJmeno("Karel");
    cout << s1->getJmeno() << endl;
    return 0;
}