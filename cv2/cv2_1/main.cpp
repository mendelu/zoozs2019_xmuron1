#include <iostream>
using namespace std;

class Student{
public:
    string m_jmeno;
    int m_id;

    Student() {
        m_jmeno = "Bezejmena";
        m_id = 0;
    }

    Student(string nove_jmeno){
        m_jmeno = nove_jmeno;
        m_id = 0;
    }
    Student(string nove_jmeno,int nove_id){
        m_jmeno = nove_jmeno;
        m_id = nove_id;
    }

    string getJmeno(){
        return m_jmeno;
    }

    void nastavJmeno(string nove_jmeno){
        m_jmeno = nove_jmeno;
    }

    int getId(){
        return m_id;
    }
};

int main() {
    Student* s1 = new Student("Pavel");
    cout << s1->getJmeno() << " " << s1->getId() << endl;

    Student* s2 = new Student();
    cout << s2->getJmeno() << " " << s2->getId() << endl;

    Student* s3 = new Student("Mik",5);
    cout << s3->getJmeno() << " " << s3->getId() << endl;

    return 0;
}