//
//  main.cpp
//  ZOO_02
//
//  Created by David on 03/10/2019.
//  Copyright © 2019 David Prochazka. All rights reserved.
//

// Vytvorte IS posty.
// Je potreba mit moznost evidovat baliky. Baliky se eviduji vice zpusoby. Bud se preda kuryrovi a ten jen zaeviduje adresu, pozdeji se zvazi. Nebo se preda u prepazky, kde se i zvazi a muze se zadat kontakt. Vazu je vzdy potreba mit moznost zadat i zpetne kvuli kurirovi...

#include <iostream>
using namespace std;

class Parcel{
    string m_shippingAddress;
    string m_notificationEmail;
    string m_notificationPhone;
    float m_weight;

public:
    // Nekdo da balik kuryrovi, ten ho zaeviduje, ale nevazi
    Parcel(string shippingAddress){
        // zduraznit, ze je potreba vzdy inicializovat vsechny atributy objektu!
        m_shippingAddress = shippingAddress; // pozdejij setShipping...
        m_notificationEmail = "";
        m_notificationPhone = "";
        m_weight = 0.0;
    }

    // Nekdo da balik na postu, kde ho zvazi a zaeviduji. Dotycny nechtel notifikaci
    Parcel(string shippingAddress, float weight){
        m_shippingAddress = shippingAddress;
        m_notificationEmail = "";
        m_notificationPhone = "";
        m_weight = weight;
    }

    // Nekdo da balik na postu, kde ho zvazi a zaeviduji. Dotycny chtel notifikaci
    Parcel(string shippingAddress, float weight, string notificationEmail, string notificationPhone){
        m_shippingAddress = shippingAddress;
        m_notificationEmail = notificationEmail;
        m_notificationPhone = notificationPhone;
        m_weight = weight;
    }

    // Nekdo da balik na postu, kde ho zvazi a zaeviduji. Dotycny chtel notifikaci jen mailem. Nejde udelat varianta jen pro telefon, protoze je to oboje string.
    Parcel(string shippingAddress, float weight, string notificationEmail){
        m_shippingAddress = shippingAddress;
        m_notificationEmail = notificationEmail;
        m_notificationPhone = "";
        m_weight = weight;
    }

    // testovaci metoda
    void printInfo(){
        cout << "Address:" << m_shippingAddress << endl;
        cout << "Email:" << m_notificationEmail << endl;
        cout << "Phone:" << m_notificationPhone << endl;
        cout << "Weight:" << m_weight << endl;
    }

    // jako rozsireni lze zacit testovat hodnoty, ktere predavame (pro sikovne)
    // takoveto kontrolni metody pak muzeme volat z konstruktoru misto, abychom to 5x testovali primo v nich.
    void setShippingAddress(string shippingAddress){
        if (shippingAddress != ""){
            m_shippingAddress = shippingAddress;
        } else {
            // hodne naivni zpracovani chyby, ale lepsi, nez nic
            cout << "Sorry, you cannot have empty address" << endl;
            m_shippingAddress = "Not set!";
        }
    }

    void setWeight(float weight){
        if (weight > 0.0){
            m_weight = weight;
        } else {
            // hodne naivni zpracovani chyby, ale lepsi, nez nic
            cout << "Sorry, you must have weight larger than 0." << endl;
            m_weight = 0.0;
        }
    }
};

int main() {
    Parcel* myNewPhone = new Parcel("Zemedelska 1, Brno", 1.5);
    myNewPhone->printInfo();

    delete myNewPhone;
    return 0;
}