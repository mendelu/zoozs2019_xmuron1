#include <iostream>
using namespace std;

class Clovek {
public:
    string m_jmeno;

    void setJmeno(string noveJmeno){
        m_jmeno = noveJmeno;
    }

    string getJmeno(){
        return m_jmeno;
    }
};

int main() {
    Clovek* c1 = new Clovek();
    Clovek* c2 = new Clovek();
    c1->setJmeno("Pavel");
    c2->setJmeno("Eva");
    cout << c1->getJmeno() << " " << c2->getJmeno();
    return 0;
}