//
// Created by xmuron1 on 13.11.2019.
//

#include "Kniha.h"

Kniha::Kniha(string autor, string nazev) {
    Kniha::s_pocitadlo += 1;
    m_autor = autor;
    m_nazev = nazev;
    m_id = Kniha::s_pocitadlo;
}

int Kniha::getId() {
    return m_id;
}

string Kniha::getAutor() {
    return m_autor;
}

string Kniha::getNazev() {
    return m_nazev;
}

int Kniha::s_pocitadlo = 0;