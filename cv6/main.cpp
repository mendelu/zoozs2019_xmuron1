#include <iostream>
#include "Kniha.h"
#include "IS.h"

int main() {
    IS* prvni = new IS();
    IS* druhy = new IS();

    prvni->vytvorKnihu("ds","ds");
    druhy->vypisKnihy();

    /*
    IS::vytvorKnihu("Nemcova","Babicka");
    Kniha* k1 = new Kniha("Capek","Dasenka");
    IS::pridejKnihu(k1);

    IS::vypisKnihy();
     */
    return 0;
}