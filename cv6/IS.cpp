//
// Created by xmuron1 on 13.11.2019.
//

#include "IS.h"

void IS::pridejKnihu(Kniha *kniha) {
    IS:s_knihy.push_back(kniha);
}

void IS::vytvorKnihu(string autor, string nazev) {
    Kniha* k = new Kniha(autor,nazev);
    IS::s_knihy.push_back(k);
}

void IS::vypisKnihy() {
    for(Kniha* k: IS::s_knihy){
        cout << k->getNazev() << " " << k->getAutor() << " " << k->getId() << endl;
    }
}

vector<Kniha*> IS::s_knihy = {};