//
// Created by xmuron1 on 13.11.2019.
//
#include "Kniha.h"
#include <vector>

#ifndef CV5_IS_H
#define CV5_IS_H

class IS {
private:
    static vector<Kniha*> s_knihy;
public:
    static void vytvorKnihu(string autor, string nazev);
    static void pridejKnihu(Kniha* kniha);
    static void vypisKnihy();


};


#endif //CV5_IS_H
