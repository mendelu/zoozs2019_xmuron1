//
// Created by xmuron1 on 13.11.2019.
//
#include <iostream>

#ifndef CV5_KNIHA_H
#define CV5_KNIHA_H

using namespace std;
class Kniha {
private:
    string m_autor;
    string m_nazev;
    int m_id;
    static int s_pocitadlo;
public:
    Kniha(string autor,string nazev);
    string getAutor();
    string getNazev();
    int getId();
};


#endif //CV5_KNIHA_H
