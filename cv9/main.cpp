//
//  main.cpp
//  ZOO_cv09_factory
//
//  Created by David on 28/11/2019.
//  Copyright © 2019 David Prochazka. All rights reserved.
//

#include "StudyFactory.hpp"
#include "BachelorFactory.hpp"

int main(int argc, const char * argv[]) {
    // tady muzeme dat uzivateli treba vybrat jakou tovarnu chce vytvorit, ja mam jen jednu...
    StudyFactory* factory = new BachelorFactory();
    
    // uzivatel si uz nemuze vybrat jaky typ vytvori, pouze bakalari
    Student* bcStudent = factory->createStudent("David");
    Course* bcCourse = factory->createCourse("ZOO", 5);

    delete factory;
    delete bcStudent;
    delete bcCourse;

    return 0;
}
