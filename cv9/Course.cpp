//
//  Course.cpp
//  ZOO_cv09_factory
//
//  Created by David on 28/11/2019.
//  Copyright © 2019 David Prochazka. All rights reserved.
//

#include "Course.hpp"

Course::Course(StudyType type, std::string name, int credits){
    m_type = type;
    m_name = name;
    m_credits = credits;
}

StudyType Course::getStudyType(){
    return m_type;
}

int Course::getCredits(){
    return m_credits;
}

std::string Course::getName(){
    return m_name;
}
