//
//  Course.hpp
//  ZOO_cv09_factory
//
//  Created by David on 28/11/2019.
//  Copyright © 2019 David Prochazka. All rights reserved.
//

#ifndef Course_hpp
#define Course_hpp

#include <iostream>
#include "StudyType.hpp"

class Course {
    StudyType m_type;
    std::string m_name;
    int m_credits;
    
public:
    Course(StudyType type, std::string name, int credits);
    StudyType getStudyType();
    int getCredits();
    std::string getName();
};


#endif /* Course_hpp */
