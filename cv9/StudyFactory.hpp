//
//  StudyFactory.hpp
//  ZOO_cv09_factory
//
//  Created by David on 28/11/2019.
//  Copyright © 2019 David Prochazka. All rights reserved.
//

#ifndef StudyFactory_hpp
#define StudyFactory_hpp

#include "Student.hpp"
#include "Course.hpp"

/**
 Tato trida predstavuje sablonu, jak ma vypadat libovolna tovarna, ktera je pro nejaky typ studia. Musi umet vytvaret studenty a kurzy.
 */

class StudyFactory {
public:
    virtual Student* createStudent(std::string name) = 0;
    virtual Course* createCourse(std::string name, int credits) = 0;
    virtual ~StudyFactory(){}; // nezapominat na virtualni destruktory jinak se nebudou provolavat destruktory potomku
};

#endif /* StudyFactory_hpp */
