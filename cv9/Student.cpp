//
//  Student.cpp
//  ZOO_cv09_factory
//
//  Created by David on 28/11/2019.
//  Copyright © 2019 David Prochazka. All rights reserved.
//

#include "Student.hpp"

Student::Student(std::string name, float scholarshipPerYear, int standardStudyLenght, bool mealDiscount){
    m_name = name;
    m_scholarshipPerYear = scholarshipPerYear;
    m_standardStudyLenght = standardStudyLenght;
    m_mealDiscount = mealDiscount;
}

Student* Student::createStudent(std::string name, StudyType study){
    Student* newStudent = nullptr;
    
    // se switchem to neprehanet, ale obcas je lepsi, nez if-elsy
    switch (study) {
        case StudyType::Bc:
            newStudent = new Student(name, 1000, 3, true);
            break; // nezapomenout na break
        case StudyType::Mgr:
            newStudent = new Student(name, 2000, 2, true);
            break;
        case StudyType::Phd:
            newStudent = new Student(name, 1000, 3, false);
            break;
        default:
            std::cout << "Terrible thing happened!" << std::endl;
            break;
    }
    
    return newStudent;
}

std::string Student::getName(){
    return m_name;
}

float Student::getScholarshipPerYear(){
    return m_scholarshipPerYear;
}

int Student::getStandardStudyLenght(){
    return m_standardStudyLenght;
}

bool Student::getMealDiscount(){
    return m_mealDiscount;
}
