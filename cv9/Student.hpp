//
//  Student.hpp
//  ZOO_cv09_factory
//
//  Created by David on 28/11/2019.
//  Copyright © 2019 David Prochazka. All rights reserved.
//

#ifndef Student_hpp
#define Student_hpp

#include <iostream>
#include "StudyType.hpp"

class Student {
    std::string m_name;
    float m_scholarshipPerYear;
    int m_standardStudyLenght;
    bool m_mealDiscount;

    Student(std::string name, float scholarshipPerYear, int standardStudyLenght, bool mealDiscount);

public:
    static Student* createStudent(std::string name, StudyType study);
    
    std::string getName();
    float getScholarshipPerYear();
    int getStandardStudyLenght();
    bool getMealDiscount();
};


#endif /* Student_hpp */
