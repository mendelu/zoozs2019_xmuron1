//
//  BachelorFactory.hpp
//  ZOO_cv09_factory
//
//  Created by David on 28/11/2019.
//  Copyright © 2019 David Prochazka. All rights reserved.
//

#ifndef BachelorFactory_hpp
#define BachelorFactory_hpp

#include "Student.hpp"
#include "Course.hpp"
#include "StudyFactory.hpp"

/**
 Tady pak implementujeme jednotlive varianty
 */

class BachelorFactory: public StudyFactory {
public:
    Student* createStudent(std::string name);
    Course* createCourse(std::string name, int credits);
};

#endif /* BachelorFactory_hpp */
