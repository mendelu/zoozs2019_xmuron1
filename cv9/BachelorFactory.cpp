//
//  BachelorFactory.cpp
//  ZOO_cv09_factory
//
//  Created by David on 28/11/2019.
//  Copyright © 2019 David Prochazka. All rights reserved.
//

#include "BachelorFactory.hpp"

Student* BachelorFactory::createStudent(std::string name){
    return Student::createStudent(name, StudyType::Bc);
}

Course* BachelorFactory::createCourse(std::string name, int credits){
    return new Course(StudyType::Bc, name, credits);
}
