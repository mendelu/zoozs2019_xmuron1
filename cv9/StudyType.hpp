//
//  StudyType.h
//  ZOO_cv09_factory
//
//  Created by David on 28/11/2019.
//  Copyright © 2019 David Prochazka. All rights reserved.
//

#ifndef StudyType_h
#define StudyType_h

enum class StudyType {
    Bc,
    Mgr,
    Phd,
};

#endif /* StudyType_h */
