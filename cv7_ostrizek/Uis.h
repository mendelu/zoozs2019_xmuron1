//
// Created by xostrize on 20.11.2019.
//

#ifndef CV7_DEDICNOST_UIS_H
#define CV7_DEDICNOST_UIS_H

#include <iostream>
#include <vector>
#include "Ucitel.h"
#include "Student.h"

using namespace std;

class Uis {
private:
    vector<Ucitel*> m_ucitele;
    vector<Student*> m_studenti;
public:
    Uis();
    void addStudent(string jmeno, string rodneCislo, int semestr, float prumer);
    void addUcitel(string jmeno, string rodneCislo, string ustav);
    void printInfo();
    ~Uis();

};


#endif //CV7_DEDICNOST_UIS_H
