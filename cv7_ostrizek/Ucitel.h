//
// Created by xostrize on 20.11.2019.
//

#ifndef CV7_DEDICNOST_UCITEL_H
#define CV7_DEDICNOST_UCITEL_H

#include <iostream>
#include "Osoba.h"

using namespace std;


class Ucitel: public Osoba {

private:
    string m_ustav;
public:
    Ucitel(string jmeno, string rodneCislo, string ustav);
    string getUstav();
    void setUstav(string ustav);
    void printInfo();

};


#endif //CV7_DEDICNOST_UCITEL_H
