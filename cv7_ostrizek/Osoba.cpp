//
// Created by xostrize on 20.11.2019.
//

#include "Osoba.h"

Osoba::Osoba(string jmeno, string rodneCislo){
    setJmeno(jmeno);
    m_rodneCislo=rodneCislo;
}
void Osoba::printInfo(){
    cout<<"Osoba -----------------" << endl;
    cout<< "jmeno: \t\t" << m_jmeno << endl;
    cout<< "rodne Cislo: \t" << m_rodneCislo << endl;

}
void Osoba::setJmeno(string jmeno){
    m_jmeno=jmeno;
}
string Osoba::getJmeno(){
    return m_jmeno;
}
string Osoba::getRodneCislo(){
    return m_rodneCislo;
}