//
// Created by xostrize on 20.11.2019.
//

#ifndef CV7_DEDICNOST_OSOBA_H
#define CV7_DEDICNOST_OSOBA_H

#include <iostream>

using namespace std;

class Osoba {
private:
    string m_jmeno;
    string m_rodneCislo;
public:
    Osoba(string jmeno, string rodneCislo);
    void printInfo();
    void setJmeno(string jmeno);
    string getJmeno();
    string getRodneCislo();
};


#endif //CV7_DEDICNOST_OSOBA_H
