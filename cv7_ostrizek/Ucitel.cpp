//
// Created by xostrize on 20.11.2019.
//

#include "Ucitel.h"


Ucitel::Ucitel(string jmeno, string rodneCislo, string ustav)
    :Osoba::Osoba(jmeno, rodneCislo){
    setUstav(ustav);
}
string Ucitel::getUstav(){
    return m_ustav;
}
void Ucitel::setUstav(string ustav){
    m_ustav=ustav;
}


void Ucitel::printInfo(){
    cout<<"Ucitel -------------" << endl;
    Osoba::printInfo();
    cout<< "ustav: \t\t" << getUstav() << endl;
}