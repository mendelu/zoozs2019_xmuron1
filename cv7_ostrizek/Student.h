//
// Created by xostrize on 20.11.2019.
//

#ifndef CV7_DEDICNOST_STUDENT_H
#define CV7_DEDICNOST_STUDENT_H

#include <iostream>
#include "Osoba.h"

using namespace std;
class Student: public Osoba {
private:
    int m_semestr;
    float m_prumer;
public:
    Student(string jmeno, string rodneCislo, int semestr, float prumer);
    void zvysSemestr();
    int getSemestr();
    float getPrumer();
    void setPrumer(float prumer);
    void printInfo();
};


#endif //CV7_DEDICNOST_STUDENT_H
