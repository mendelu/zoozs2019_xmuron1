#include <iostream>
#include "Osoba.h"
#include "Ucitel.h"
#include "Student.h"

int main() {

    Osoba* osoba= new Osoba("Frantisek", "123456759");
    osoba->printInfo();
    delete(osoba);

    Ucitel* ucitel=new Ucitel("Tomas", "987654321","Department of Informatics");
    ucitel->printInfo();

    delete(ucitel);


    Student* student= new Student("jan","654321987",2,1.2);

    student->printInfo();
    delete(student);

    Osoba* osoba1= new Student("Jakub", "1478963", 6,2);
    osoba1->printInfo();
    delete(osoba1);

    return 0;
}