//
// Created by xproch17 on 16.11.2018.
//

#ifndef CV08_PRACOVNIPOZICE_H
#define CV08_PRACOVNIPOZICE_H

class PracovniPozice {
public:
    virtual int getDniDovolene(int cerpanoDni) = 0;
    virtual int getPlat(int letVeFirme, int studenVzdelani) = 0;
};

#endif //CV08_PRACOVNIPOZICE_H
