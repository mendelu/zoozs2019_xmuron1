//
// Created by xproch17 on 16.11.2018.
//

#include "Akademik.h"

int Akademik::getDniDovolene(int cerpanoDni){
    const int maxDniDovolene = 50;

    return maxDniDovolene-cerpanoDni;
}

int Akademik::getPlat(int letVeFirme, int studenVzdelani){
    const int zakladPlatu = 40000;
    const int bonusZaStupen = 5000;

    return zakladPlatu + studenVzdelani * bonusZaStupen;
}