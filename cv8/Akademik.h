//
// Created by xproch17 on 16.11.2018.
//

#ifndef CV08_AKADEMIK_H
#define CV08_AKADEMIK_H

#include "PracovniPozice.h"

class Akademik: public PracovniPozice {
public:
    int getDniDovolene(int cerpanoDni);
    int getPlat(int letVeFirme, int studenVzdelani);
};

#endif //CV08_AKADEMIK_H
