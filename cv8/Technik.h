//
// Created by xproch17 on 16.11.2018.
//

#ifndef CV08_TECHNIK_H
#define CV08_TECHNIK_H

#include "PracovniPozice.h"

class Technik: public PracovniPozice {
    // pokud chci aby vsechny metody vsech obj. videly
    //static const int s_maxDniDovolene = 30; // level 3

public:
    int getDniDovolene(int cerpanoDni);
    int getPlat(int letVeFirme, int studenVzdelani);
};


#endif //CV08_TECHNIK_H
