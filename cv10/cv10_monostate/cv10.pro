TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    SpravceZakazniku.cpp \
    Zakaznik.cpp

HEADERS += \
    SpravceZakazniku.h \
    Zakaznik.h
