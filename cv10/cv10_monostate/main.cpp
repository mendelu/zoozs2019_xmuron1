#include <iostream>
#include "SpravceZakazniku.h"
#include "Zakaznik.h"

int main()
{
    is::Zakaznik* karel
        = is::SpravceZakazniku::createZakaznik("Karel");//new Zakaznik("",0);
    is::Zakaznik* karel2
        = is::SpravceZakazniku::createZakaznik("Karel");//new Zakaznik("",0);

    std::cout << "Jmeno: " << karel->getJmeno() << std::endl;
    std::cout << "id: " << karel->getId() << std::endl;

    is::Zakaznik* nalezenyZakaznik
        = is::SpravceZakazniku::getZakaznikById(0);

    std::cout << "Jmeno: " << nalezenyZakaznik->getJmeno() << std::endl;

    std::vector<is::Zakaznik*> nalezeniZakaznici
        = is::SpravceZakazniku::getZakaznikByName("Karel");
    std::cout << "Kolik zakazniku bylo nalezeno: "
              << nalezeniZakaznici.size() << std::endl;

    for (is::Zakaznik* zak:nalezeniZakaznici){
        std::cout << zak->getId() << std::endl;
    }

    return 0;
}
