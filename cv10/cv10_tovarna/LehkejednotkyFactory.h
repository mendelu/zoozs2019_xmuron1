#ifndef LehkejednotkyFactory_H
#define LehkejednotkyFactory_H

#include "JednotkyFactory.h"
#include "LehkoOdenec.h"

namespace rytiri {
	class LehkejednotkyFactory : public rytiri::JednotkyFactory {

	public:
		int m_vahaHelmy;

		rytiri::Rytir* getRytir(string jmeno, int sila);

		LehkejednotkyFactory(int vahaHelmyLehkoodence);
	};
}
#endif // LehkejednotkyFactory_H
