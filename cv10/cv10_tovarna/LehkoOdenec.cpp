#include "LehkoOdenec.h"

rytiri::LehkoOdenec::LehkoOdenec(string jmeno, int sila, int vahaHelmy):Rytir(jmeno,sila) {
	m_vahaHelmy = vahaHelmy;
}

int rytiri::LehkoOdenec::getUtok() {
	return getSila() * 2;
}

int rytiri::LehkoOdenec::getObrana() {
	return m_vahaHelmy;
}

void rytiri::LehkoOdenec::setVahaHelmy(int silaHelmy) {
	m_vahaHelmy = silaHelmy;
}
