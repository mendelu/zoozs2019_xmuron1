#include <iostream>

#include "Rytir.h"
#include "LehkoOdenec.h"
#include "JednotkyFactory.h"
#include "TezkejednotkyFactory.h"
#include "LehkejednotkyFactory.h"

using namespace std;
using namespace rytiri;

int main()
{
    Rytir* jason = new LehkoOdenec("Jason",5,8);
    cout << "obrana: " << jason->getObrana() << endl;
    cout << "utok: " << jason->getUtok() << endl;
    delete jason;

    JednotkyFactory* tovarna = new TezkejednotkyFactory(20);
    Rytir* r1 = tovarna->getRytir("krason",10);
    Rytir* r2 = tovarna->getRytir("mason",12);
    delete tovarna;
    tovarna = new LehkejednotkyFactory(10);
    Rytir* r3 = tovarna->getRytir("krason",10);
    Rytir* r4 = tovarna->getRytir("mason",12);
    delete tovarna;

    delete r1;
    delete r2;
    delete r3;
    delete r4;
    return 0;
}
