#ifndef Rytir_H
#define Rytir_H

#include <iostream>
using namespace std;

namespace rytiri {
	class Rytir {

	private:
		string m_jmeno;
		int m_sila;

	public:
		Rytir(string jmeno, int sila);

		virtual int getUtok() = 0;

		virtual int getObrana() = 0;

		int getSila();
	};
}
#endif // Rytir_H
