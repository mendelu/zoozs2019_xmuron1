#ifndef JednotkyFactory_H
#define JednotkyFactory_H

#include "Rytir.h"

namespace rytiri {
	class JednotkyFactory {

	public:
		virtual rytiri::Rytir* getRytir(string jmeno, int sila) = 0;
	};
}
#endif // JednotkyFactory_H
