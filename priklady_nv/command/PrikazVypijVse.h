#ifndef PRIKAZVYPIJVSE_H
#define PRIKAZVYPIJVSE_H
#include <iostream>
#include "Prikaz.h"

using namespace std;
class PrikazVypijVse : public Prikaz {

public:
	void PouzijLektvar(Lektvar* lektvar, Hrdina* hrdina);

	string getPopis();
};

#endif // PRIKAZVYPIJVSE_H
