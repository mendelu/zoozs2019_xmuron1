#include "PrikazUpijTrochu.h"

void PrikazUpijTrochu::PouzijLektvar(Lektvar* lektvar, Hrdina* hrdina) {
    int kolikBonus = lektvar->getBonusZivot();
    hrdina->setZivot(2);
    lektvar->setBonusZivot(kolikBonus-2);
    cout<<"zivot se ti na vysil o 2 na: "<<hrdina->getZivot()<<endl;
}

string PrikazUpijTrochu::getPopis() {
    return "trochu ses napil, zivot se zvysil o 2";
}
