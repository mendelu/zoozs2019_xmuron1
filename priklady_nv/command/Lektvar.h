#ifndef LEKTVAR_H
#define LEKTVAR_H
#include <iostream>

using namespace std;
class Lektvar {

private:
	int m_bonusZivota;
	string m_popis;

public:
	Lektvar(int bonus, string popis);

	void printInfo();

	int getBonusZivot();

	void setBonusZivot(int kolik);
};
#endif // LEKTVAR_H

