#include <iostream>

#include "Hrdina.h"
#include "PrikazProhledni.h"
#include "PrikazUpijTrochu.h"
#include "PrikazVypijVse.h"

using namespace std;

int main()
{
    Hrdina* hulk = new Hrdina(100);

    hulk->seberLektvar(new Lektvar(10, "cervenej hnus"));
    hulk->seberLektvar(new Lektvar(80, "ambrozie"));
    hulk->seberLektvar(new Lektvar(40, "cajicek"));

    hulk->naucSePrikaz(new PrikazProhledni());
    hulk->naucSePrikaz(new PrikazUpijTrochu());
    hulk->naucSePrikaz(new PrikazVypijVse());

    //hulk->vypisPrikazy();


    hulk->pouzijLektvar();
    //hulk->pouzijLektvar();
    //hulk->pouzijLektvar();

    delete hulk;

    return 0;
}
