#ifndef ENGINE
#define ENGINE
#include<iostream>
#include<vector>

using namespace std;
class Engine {

private:
	vector<string> m_planety;
	static Engine* s_engine;

	Engine();

public:
	static Engine* getInstance();

	void addPlaneta(string jaka);

	void vypisInfo();
};
#endif // ENGINE

